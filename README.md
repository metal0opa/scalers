## How to run the app

1. Create a .env file in the backend directory and add the InfluxDB credentials similar to the .env.example file.
2. Start the backend server by running the following commands in the backend directory:

```bash
python app.py
```

3. Start the frontend server by running the following commands in the frontend directory:

```bash
npm install
npm start
```

4. Open http://localhost:3000/ in your browser to view the app.
