# create a flask app with a get endpoint

from flask import Flask, jsonify
import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from flightsql import FlightSQLClient
from influxdb_client.client.write_api import SYNCHRONOUS
from dotenv import load_dotenv

load_dotenv()

token = os.environ.get("INFLUXDB_TOKEN")

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/query")
def query():
    org = "scalers"
    url = "https://us-east-1-1.aws.cloud2.influxdata.com"
    bucket = "scalers"
    query = """SELECT *
    FROM 'census'
    """
    # Define the query client
    query_client = FlightSQLClient(
        host="us-east-1-1.aws.cloud2.influxdata.com",
        token=token,
        metadata={"bucket-name": bucket},
        org=org,
        features={"metadata-reflection": "true"},
    )

    # Execute the query
    info = query_client.execute(query)
    reader = query_client.do_get(info.endpoints[0].ticket)

    # Convert to dataframe
    data = reader.read_all()
    df = data.to_pandas().sort_values(by="time")
    return jsonify(df.to_dict())


if __name__ == "__main__":
    app.run(
        host="localhost",
        port=8080,
        debug=True,
    )

# Run the flask app
# $ python flask.py

# In another terminal, run the following curl command to query the data
# $ curl http://localhost:8080/query
