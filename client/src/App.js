import React, { useState, useEffect } from "react";
import { InfluxDB } from "@influxdata/influxdb-client-browser";
import {
  Chart,
  LineController,
  CategoryScale,
  LinearScale,
  LineElement,
  PointElement,
} from "chart.js";
import axios from "axios";

const App = () => {
  // Keep track of Chart instances created on each canvas
  const charts = {};
  const [sensorData, setSensorData] = useState([]);
  Chart.register(LineController);
  Chart.register(CategoryScale);
  Chart.register(LinearScale);
  Chart.register(PointElement);
  Chart.register(LineElement);

  // Create a Chart instance
  function createChart(canvas, data) {
    const ctx = canvas.getContext("2d");
    const id = canvas.id;

    // Destroy old Chart instance
    if (charts[id]) {
      charts[id].destroy();
    }

    // Create new Chart instance
    charts[id] = new Chart(ctx, {
      type: "line",
      data: data,
    });

    return charts[id];
  }

  useEffect(() => {
    // Connect to InfluxDB and retrieve sensor data
    const influx = new InfluxDB({
      url: "https://us-east-1-1.aws.cloud2.influxdata.com",
      writeOptions: {
        batchSize: 100,
        flushInterval: 10_000,
      },
    });
    // get the data from localhost port 8080
    axios
      .get("http://localhost:8080/query")
      .then((res) => {
        console.log(res.data);
        setSensorData(res.data);
      })
      .catch((err) => {
        console.error(err);
      });

    const chartInstance = createChart(
      document.getElementById("chart"),
      sensorData
    );

    // Generate dummy data every second and store it in InfluxDB
    const interval = setInterval(() => {
      const now = new Date().toISOString();
      const value = Math.floor(Math.random() * 100);

      // Store the dummy data in InfluxDB
      influx
        .write("sensor_data")
        .field({ time: now, value: value })
        .then(() => {
          // Update the chart with the new data
          const newData = [...sensorData, { x: now, y: value }];
          setSensorData(newData);
          chartInstance.data.datasets[0].data = newData;
          chartInstance.update();
        });
    }, 1000);

    // Clean up interval on unmount
    return () => clearInterval(interval);
  }, [sensorData]);

  // Dummy data for sensor names and types
  const sensorDetails = [
    { name: "Sensor 1", type: "Temperature" },
    { name: "Sensor 2", type: "Humidity" },
    { name: "Sensor 3", type: "Pressure" },
    { name: "Sensor 4", type: "Light" },
    { name: "Sensor 5", type: "Noise" },
    { name: "Sensor 6", type: "Vibration" },
    { name: "Sensor 7", type: "Accelerometer" },
    { name: "Sensor 8", type: "Gyroscope" },
    { name: "Sensor 9", type: "Magnetometer" },
    { name: "Sensor 10", type: "CO2" },
  ];

  return (
    <div>
      <canvas id="chart"></canvas>
      {sensorData.map((data, index) => (
        <div key={index}>
          <p>Name: {sensorDetails[index].name}</p>
          <p>Type: {sensorDetails[index].type}</p>
          <p>Last Reading: {data.y}</p>
        </div>
      ))}
    </div>
  );
};

export default App;
